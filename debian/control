Source: kf5-kdepim-apps-libs
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 2.8.12~),
               debhelper (>= 9),
               extra-cmake-modules (>= 5.19.0~),
               libgpgme-dev | libgpgme11-dev,
               libgrantlee5-dev (>= 5.0~),
               libkf5akonadi-dev (>= 4:5.2.0~),
               libkf5akonadicontact-dev (>= 5.2.0~),
               libkf5akonadimime-dev (>= 5.2.0~),
               libkf5completion-dev (>= 5.19.0~),
               libkf5config-dev (>= 5.19.0~),
               libkf5contacteditor-dev (>= 17.04.0~),
               libkf5contacts-dev (>= 5.2.0~),
               libkf5coreaddons-dev (>= 5.19.0~),
               libkf5dbusaddons-dev (>= 5.19.0~),
               libkf5grantleetheme-dev (>= 5.2.0~),
               libkf5i18n-dev (>= 5.19.0~),
               libkf5iconthemes-dev (>= 5.19.0~),
               libkf5libkleo-dev (>= 5.2.0~),
               libkf5mime-dev (>= 5.2.0~),
               libkf5pimcommon-dev (>= 5.2.0~),
               libkf5pimtextedit-dev (>= 5.2.0~),
               libkf5prison-dev,
               libkf5service-dev (>= 5.19.0~),
               libkf5sonnet-dev (>= 5.19.0~),
               libkf5webkit-dev (>= 5.19.0~),
               libkf5widgetsaddons-dev (>= 5.19.0~),
               libkf5xmlgui-dev (>= 5.19.0~),
               libqt5webkit5-dev (>= 5.4.0~),
               pkg-kde-tools (>= 0.12),
               qtbase5-dev (>= 5.4.0~),
               qttools5-dev (>= 5.4.0~)
Standards-Version: 3.9.8
Homepage: http://pim.kde.org/
Vcs-Browser: https://anonscm.debian.org/git/pkg-kde/applications/kdepim-apps-libs.git
Vcs-Git: https://anonscm.debian.org/git/pkg-kde/applications/kdepim-apps-libs.git

Package: kf5-kdepim-apps-libs-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: libkf5composereditorng5 (<= 4:16.04), ${kde-l10n:all}
Replaces: libkf5composereditorng5 (<= 4:16.04), ${kde-l10n:all}
Description: KDE PIM mail related libraries, data files
 This package is part of the KDE PIM module.

Package: libkf5kaddressbookgrantlee5
Architecture: any
Multi-Arch: same
Depends: kf5-kdepim-apps-libs-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: libkf5libkdepim5 (<= 4:16.04)
Replaces: libkf5libkdepim5 (<= 4:16.04)
Description: grantlee library
 This package is part of the KDE PIM module.

Package: libkf5kaddressbookgrantlee-dev
Section: libdevel
Architecture: any
Depends: libgrantlee5-dev (>= 5.0~),
         libkf5akonadicontact-dev (>= 5.2.0~),
         libkf5kaddressbookgrantlee5 (= ${binary:Version}),
         libkf5pimcommon-dev (>= 5.2.0~),
         ${misc:Depends},
         ${shlibs:Depends}
Description: grantlee library, devel files
 This package is part of the KDE PIM module.

Package: libkf5kaddressbookimportexport5
Architecture: any
Multi-Arch: same
Depends: kf5-kdepim-apps-libs-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Addressbook import export library
 This package is part of the KDE PIM module.

Package: libkf5kaddressbookimportexport-dev
Section: libdevel
Architecture: any
Depends: libkf5akonadi-dev (>= 4:5.2.0~),
         libkf5config-dev (>= 5.19.0~),
         libkf5kaddressbookimportexport5 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Addressbook import export library, devel files
 This package is part of the KDE PIM module.

Package: libkf5sendlater-dev
Section: oldlibs
Priority: extra
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: dummy package which can be removed
 Dummy package for an old lib this can be removed.

Package: libkf5kdepimdbusinterfaces-dev
Section: oldlibs
Priority: extra
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: dummy package which can be removed
 Dummy package for an old lib this can be removed.

Package: libkf5followupreminder-dev
Section: oldlibs
Priority: extra
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: dummy package which can be removed
 Dummy package for an old lib this can be removed.
